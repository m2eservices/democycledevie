package com.example.jean_claude.demo_cycledevie_activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        afficheMessage("create");
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        afficheMessage("start");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        afficheMessage("stop");
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        afficheMessage("resume");
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        afficheMessage("restart");
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        afficheMessage("pause");

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        afficheMessage("destroy");
    }

    public void afficheMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
